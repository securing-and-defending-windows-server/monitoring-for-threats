This is a quick guide, and process, to look and monitor for threats on a windows PC/Server.

# Microsoft File Signature Verification

sigverif.exe 

Verifys all core OS .dll files are legitimate, and that the hashes match from microsoft.
If any files are inconsitent they are flaged and or replaced.


https://www.thewindowsclub.com/identify-unsigned-drivers-sigverif-windows


# SYS Internals 
    - rootkit revealer
    - AccessChk
    - AutoRuns
    - sysmon
    - sigcheck
    - PSLoggedOn
    - Process Explorer
    - Process Monitor
    - variety of tools within the suit, check it out, as it changes with updates.
    

# Sysmon notable event ID's
    - Event ID 1 - Creation of a new process
    - Event ID 2 - Creation of a new file
    - Event ID 3 - New network connection detected
    - Event ID 5 - Process Ended
    - Event ID 6 - Driver Loaded
    - Event ID 7 - Image Loaded
    - Event ID 8 - Remote thread creation detected
    - Event ID 9 - RawAccessRead
    - Event ID 10- ProcessAccess
    - Event ID 11- FileCreate
    - Event ID 12- Registry Event (object create and delete)
    - Event ID 13- RegistryEvent (Value Set)
    - Event ID 14- RegistryEvent (Key and Value Rename)
    - Event ID 15- FileCreateStreamHash
    - Event ID 17- PipeEvent (Pipe Created)
    - Event ID 18- PipeEvent (Pipe Connected)
    - Event ID 19- WmiEvent (WmiEventFilter activity detected)
    - Event ID 20- WmiEvent (WmiEventConsumer activity detected)
    - Event ID 21- WmiEvent (WmiEventConsumerToFilter activity)
    - Event ID 255- Error